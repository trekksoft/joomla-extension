<?php
/*
 * This file gets description from base extension (about.html file) and puts it inside
 * joomla's description tag at trekksoft.xml
 *
 * (it is executed automatically on composer update)
 */

$replaced = preg_replace_callback('/<description>(.*)<\/description>/', function($matches){
    return '<description><![CDATA[
        '.file_get_contents('vendor/trekksoft/base-extension/about.html')."\n".
    ']]>'.
    '</description>';
}, file_get_contents('trekksoft.xml'));


file_put_contents('trekksoft.xml', $replaced);